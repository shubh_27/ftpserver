package nxtlife.com.ftpTest.service;

import nxtlife.com.ftpTest.Extra.MyUserManagerFactory;
import nxtlife.com.ftpTest.dao.FileStackRepo;
import nxtlife.com.ftpTest.dao.userRepo;
import nxtlife.com.ftpTest.models.FileStack;
import nxtlife.com.ftpTest.models.UserView;
import nxtlife.com.ftpTest.models.ftpUser;
import org.apache.ftpserver.ftplet.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;

@Service
public class FtpService {
    @Autowired
    userRepo userRepo;

    @Autowired
    FileStackRepo fileStackRepo;

    public UserView createUser(UserView user){
        ftpUser user1=user.toEnityt();
        if(createFtpUser(user1)){
            userRepo.save(user1);
            return user;
        }else
            return null;
    }

    public List<ftpUser> getAll() { return userRepo.findAll(); }

    public boolean createFtpUser(ftpUser user) {
        MyUserManagerFactory userManagerFactory = new MyUserManagerFactory(userRepo);

        UserManager um = userManagerFactory.createUserManager();
        try
        {
            um.save(user);//Save the user to the user list on the filesystem
        }
        catch (Exception e1) {
            e1.printStackTrace();
        }
        return true;
    }

    public String findByName(String name) {
        return userRepo.findByUserName(name).getPassword();
    }

    public void readStackFiles() {
       List<FileStack> fileList=fileStackRepo.findByIsReadIsFalseOrderByTimeStamp();
       for(FileStack file:fileList){
           File f=new File("c:/ftpHome/"+file.getFileName());
           file.setRead(true);
           fileStackRepo.save(file);
       }
    }

    public void putInStack(String argument, long receivedTime) {
        FileStack fileStack=new FileStack();
        fileStack.setFileName(argument);
        fileStack.setRead(false);
        fileStack.setTimeStamp(receivedTime+"");
        fileStackRepo.save(fileStack);
    }
}