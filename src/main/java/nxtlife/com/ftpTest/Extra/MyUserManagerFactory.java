package nxtlife.com.ftpTest.Extra;

import nxtlife.com.ftpTest.dao.userRepo;
import org.apache.ftpserver.ftplet.UserManager;
import org.apache.ftpserver.usermanager.UserManagerFactory;

public class MyUserManagerFactory implements UserManagerFactory {

    private userRepo userRepo;
    public MyUserManagerFactory(userRepo userRepo){
        this.userRepo=userRepo;
    }
    @Override
    public UserManager createUserManager() {
        // custom usermanager
        return new MyUserManager(userRepo);
    }
}
