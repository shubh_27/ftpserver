package nxtlife.com.ftpTest.Extra;

import nxtlife.com.ftpTest.dao.userRepo;
import nxtlife.com.ftpTest.models.ftpUser;
import org.apache.ftpserver.ftplet.*;
import org.apache.ftpserver.usermanager.UsernamePasswordAuthentication;
import org.apache.ftpserver.usermanager.impl.AbstractUserManager;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;

@Component
public class MyUserManager extends AbstractUserManager {

    private userRepo userRepo;
    public MyUserManager(userRepo userRepo) {
        this.userRepo=userRepo;
    }

    @Override
    public User getUserByName(String s) throws FtpException {
        ftpUser user=userRepo.findByUserName(s);
        if(user==null){
            throw new AuthenticationFailedException("Authentication failed");
        }
        return  user.getBaseUser(); // returning ftp user
    }

    @Override
    public String[] getAllUserNames() throws FtpException {
        List<String> l=new ArrayList();
        for(ftpUser user:userRepo.findAll()){
            l.add(user.getUserName());
        }
        return (String[])l.toArray();
    }

    @Override
    public void delete(String s) throws FtpException {

    }

    @Override
    public void save(User user) throws FtpException {
        ftpUser user1=new ftpUser(user);
        userRepo.save(user1);
    }

    @Override
    public boolean doesExist(String s) throws FtpException {
        if(userRepo.findByUserName(s)==null)
            return false;
        return true;
    }

    @Override
    public User authenticate(Authentication authentication) throws AuthenticationFailedException {
        // add password Encryptor pending
        UsernamePasswordAuthentication upa=(UsernamePasswordAuthentication)authentication;
        ftpUser user=userRepo.findByUserNameAndPassword(upa.getUsername(),upa.getPassword());
        if(user==null){
            throw new AuthenticationFailedException();
        }
        return user.getBaseUser();
    }

    @Override
    public String getAdminName()  {
        return "admin";
    }

    @Override
    public boolean isAdmin(String s) throws FtpException {
        // to check it has role of admin
        return true;
    }
}
