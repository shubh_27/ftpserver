package nxtlife.com.ftpTest.models;

public class UserView {
    String userName;
    String homePath;
    String password;
    boolean writePermition;
    int idletime;
    boolean enableflag;

    public UserView(ftpUser user) {
        this.userName=user.userName;
        this.homePath=user.homePath;
        this.password=user.password;
        this.writePermition=user.writePermition;
        this.enableflag=user.enableflag;
        this.idletime=user.idletime;
    }

    public UserView() {
    }

    public boolean isEnableflag() {
        return enableflag;
    }

    public void setEnableflag(boolean enableflag) {
        this.enableflag = enableflag;
    }

    public int getIdletime() {
        return idletime;
    }

    public void setIdletime(int idletime) {
        this.idletime = idletime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getHomePath() {
        return homePath;
    }

    public void setHomePath(String homePath) {
        this.homePath = homePath;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isWritePermition() {
        return writePermition;
    }

    public void setWritePermition(boolean writePermition) {
        this.writePermition = writePermition;
    }

    public ftpUser toEnityt() {
        ftpUser user=new ftpUser();
        user.setHomePath(this.homePath);
        user.setUserName(userName);
        user.setPassword(password);
        user.setWritePermition(writePermition);
        user.setEnableflag(enableflag);
        user.setIdletime(idletime);
        return user;
    }
}
