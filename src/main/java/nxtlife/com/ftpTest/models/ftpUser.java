package nxtlife.com.ftpTest.models;

import org.apache.ftpserver.ftplet.Authority;
import org.apache.ftpserver.ftplet.User;
import org.apache.ftpserver.usermanager.impl.BaseUser;
import org.apache.ftpserver.usermanager.impl.ConcurrentLoginPermission;
import org.apache.ftpserver.usermanager.impl.WritePermission;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;

@Entity
public class ftpUser  extends BaseUser{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;
    String userName;
    String homePath;// should be exsist in system else it return "/"
    String password;// not encrypted
    boolean writePermition;
    int idletime; // serve disconnect it after this time completed
    boolean enableflag; // user is active or not

    public ftpUser(User user) {
        this.userName=user.getName();
        this.password=user.getPassword();
        this.idletime=user.getMaxIdleTime();
        this.writePermition=true;
        this.enableflag=user.getEnabled();
        this.homePath=user.getHomeDirectory();
    }

    public ftpUser() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getHomePath() {
        return homePath;
    }

    public void setHomePath(String homePath) {
        this.homePath = homePath;
    }


    public String getPassword() {
        return password;
    }


    public void setPassword(String password)
    {
        this.password = password;
    }

    public boolean isWritePermition() {
        return writePermition;
    }

    public void setWritePermition(boolean writePermition) {
        List<Authority> authorities = new ArrayList<Authority>();
        // if you set it 0 0 then it will show error of max login reached
        authorities.add(new ConcurrentLoginPermission(10,10));
        if(writePermition)
            authorities.add(new WritePermission()); // read permission is default
        this.writePermition = writePermition;
    }
    public List<Authority> getAuthorities(){
        List<Authority> authorities = new ArrayList<Authority>();
        authorities.add(new ConcurrentLoginPermission(10,10));
        if(writePermition)
            authorities.add(new WritePermission());
        return authorities;
    }
    public int getIdletime() {
        return idletime;
    }

    public void setIdletime(int idletime) {
        this.idletime = idletime;
    }

    public boolean isEnableflag() {
        return enableflag;
    }

    public void setEnableflag(boolean enableflag) {
        this.enableflag = enableflag;
    }

    public User getBaseUser() {
        BaseUser user=new BaseUser();
        user.setAuthorities(getAuthorities());
        user.setName(getUserName());
        user.setHomeDirectory(getHomePath());
        user.setPassword(getPassword());
        user.setEnabled(isEnableflag());
        user.setMaxIdleTime(getIdletime());
        return user;
    }
}
