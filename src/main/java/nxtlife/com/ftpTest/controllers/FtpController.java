package nxtlife.com.ftpTest.controllers;

import nxtlife.com.ftpTest.models.UserView;
import nxtlife.com.ftpTest.models.ftpUser;
import nxtlife.com.ftpTest.service.FtpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController("api/user/**")
public class FtpController {

    @Autowired
    FtpService ftpService;
    @PostMapping()
    public UserView createUser(@RequestBody UserView user){
        return ftpService.createUser(user);
    }
    @GetMapping
    public String getUser(){
        return ftpService.findByName("admin");
    }
}
