package nxtlife.com.ftpTest.config;

import nxtlife.com.ftpTest.Extra.MyUserManager;
import nxtlife.com.ftpTest.Extra.MyUserManagerFactory;
import nxtlife.com.ftpTest.dao.userRepo;
import nxtlife.com.ftpTest.models.UserView;
import nxtlife.com.ftpTest.models.ftpUser;
import nxtlife.com.ftpTest.service.FtpService;
import org.apache.ftpserver.FtpServer;
import org.apache.ftpserver.FtpServerFactory;
import org.apache.ftpserver.ftplet.*;
import org.apache.ftpserver.listener.ListenerFactory;
import org.apache.ftpserver.usermanager.PasswordEncryptor;
import org.apache.ftpserver.usermanager.PropertiesUserManagerFactory;
import org.apache.ftpserver.usermanager.impl.BaseUser;
import org.apache.ftpserver.usermanager.impl.WritePermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
public class FtpConfiguration {
    @Autowired
    FtpLet ftpLet;
    @Autowired
    userRepo userRepo;
    @PostConstruct
    public void init(){
        FtpServerFactory serverFactory = new FtpServerFactory();
        ListenerFactory factory = new ListenerFactory();
        factory.setPort(1234); // set port for ftp should be diffrent from server port
        serverFactory.addListener("default", factory.createListener());

        MyUserManagerFactory myUserManagerFactory=new MyUserManagerFactory(userRepo); // createing custom factory for usermanager and give datasource of user

        UserManager um = myUserManagerFactory.createUserManager();
        serverFactory.setUserManager(um);

        Map<String, Ftplet> m = new HashMap<String, Ftplet>();
        m.put("minaFtplet",ftpLet);// ???

        serverFactory.setFtplets(m);
        FtpServer server = serverFactory.createServer();

        try
        {
            server.start();
        }
        catch (FtpException ex) {
            ex.printStackTrace();
        }
    }

}