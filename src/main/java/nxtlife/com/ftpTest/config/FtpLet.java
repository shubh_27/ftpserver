package nxtlife.com.ftpTest.config;

import nxtlife.com.ftpTest.service.FtpService;
import org.apache.ftpserver.ftplet.*;
import org.apache.ftpserver.impl.LocalizedDataTransferFtpReply;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.io.IOException;

@Component
public class FtpLet implements Ftplet{

    @Autowired
    FtpService ftpService;

    @Override
    public void init(FtpletContext ftpletContext) throws FtpException {
        System.out.println("On init");
    }

    @Override
    public void destroy() {
        System.out.println("On destroy");
    }

    @Override
    public FtpletResult beforeCommand(FtpSession ftpSession, FtpRequest ftpRequest) throws FtpException, IOException {
        System.out.println(ftpRequest.getCommand());
        if(ftpRequest.hasArgument()){
            if(ftpRequest.getCommand().equals("STOR")){
                System.out.println("transfering file");
            }
        }
        return FtpletResult.DEFAULT;
    }

    @Override
    public FtpletResult afterCommand(FtpSession ftpSession, FtpRequest ftpRequest, FtpReply ftpReply) throws FtpException, IOException {
        System.out.println(ftpRequest.getCommand());
        if(ftpRequest.hasArgument()){
            if(ftpRequest.getCommand().equals("STOR") && ftpReply.isPositive() && ftpReply.getCode()==226){
                System.out.println("transfering complete");
                ftpService.putInStack(((LocalizedDataTransferFtpReply)ftpReply).getFile().getAbsolutePath(),ftpRequest.getReceivedTime());
            }
        }
        return FtpletResult.DEFAULT;
    }

    @Override
    public FtpletResult onConnect(FtpSession ftpSession) throws FtpException, IOException {
        System.out.println("On connect");
        return FtpletResult.DEFAULT;
    }

    @Override
    public FtpletResult onDisconnect(FtpSession ftpSession) throws FtpException, IOException {
        System.out.println("On disconnect");
        return FtpletResult.DEFAULT;
    }
}
