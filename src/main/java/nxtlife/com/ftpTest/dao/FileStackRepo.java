package nxtlife.com.ftpTest.dao;

import nxtlife.com.ftpTest.models.FileStack;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FileStackRepo extends JpaRepository<FileStack,Integer> {
    List<FileStack> findByIsReadIsFalseOrderByTimeStamp();
}
