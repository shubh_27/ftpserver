package nxtlife.com.ftpTest.dao;

import nxtlife.com.ftpTest.models.ftpUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface userRepo extends JpaRepository<ftpUser,Integer> {
    ftpUser findByUserName(String name);
    ftpUser findByPassword(String password);
    ftpUser findByUserNameAndPassword(String name,String password);
}
