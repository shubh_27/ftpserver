package nxtlife.com.ftpTest.Sechdular;

import nxtlife.com.ftpTest.service.FtpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class StackFileReadScheduler {
    @Autowired
    FtpService ftpService;
    @Scheduled(fixedRate = 10000)
    public void ReadFile(){
        ftpService.readStackFiles();
    }
}
