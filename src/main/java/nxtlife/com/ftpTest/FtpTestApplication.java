package nxtlife.com.ftpTest;

import nxtlife.com.ftpTest.config.FtpConfiguration;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.List;

@SpringBootApplication
@EnableScheduling
public class FtpTestApplication {

	public static void main(String[] args) {
		// befor start we have to configure the ftp and add scheduler to read the stack of file after same
		SpringApplication.run(FtpTestApplication.class, args);
	}
}
